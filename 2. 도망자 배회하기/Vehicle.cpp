#include "Vehicle.h"
#include  <time.h>
#include "2d/C2DMatrix.h"
#include "2d/Geometry.h"
#include "SteeringBehaviors.h"
#include "2d/Transformations.h"
#include "GameWorld.h"
#include "misc/CellSpacePartition.h"
#include "misc/cgdi.h"
#include "misc/Stream_Utility_Functions.h"

using std::vector;
using std::list;

//----------------------------- ctor -------------------------------------
//------------------------------------------------------------------------
double TotalUsedEnergy=0, NeedEnergy=0, PreyTotalUsedEnergy=0, PreyNeedEnergy=0;;
Vector2D PursuitForce;
Vector2D WaterZone = Vector2D(600, 300);
double PREDETOR_ENERGY;
double PREY_ENERGY;
double Start_Distance;
double Update_Distance;
double frictionfactor;
double PredetorSafeZone;
double PredetorScale;
double PreyScale;
Vector2D P_normalrize;
Vector2D p;
Vector2D p2;
Vector2D PredetorPos;
double seekdistance = 0;
double temp;
double Beta;
double WanderRange;
Vehicle::Vehicle(GameWorld* world,
	Vector2D position,
	double    rotation,
	Vector2D velocity,
	double    mass,
	double    max_force,
	double    max_speed,
	double    max_turn_rate,
	double    scale):    MovingEntity(position,
	scale,
	velocity,
	max_speed,
	Vector2D(sin(rotation),-cos(rotation)),
	mass,
	Vector2D(scale,scale),
	max_turn_rate,
	max_force),

	m_pWorld(world),
	m_vSmoothedHeading(Vector2D(0,0)),
	m_bSmoothingOn(true),
	m_dTimeElapsed(0.0)
{  
	InitializeBuffer();

	//set up the steering behavior class
	m_pSteering = new SteeringBehavior(this);    

	//set up the smoother
	m_pHeadingSmoother = new Smoother<Vector2D>(Prm.NumSamplesForSmoothing, Vector2D(0.0, 0.0)); 

	//GameWorld 값을 받아옴.
	frictionfactor = world->getFrictionfactor();
	Start_Distance = world->getDistance();
	Start_Distance *=2;
	TotalUsedEnergy=0;
	PreyTotalUsedEnergy=0;
	Energy=115000;
	WanderRange=20.75*3;

}


//---------------------------- dtor -------------------------------------
//-----------------------------------------------------------------------
Vehicle::~Vehicle()
{
	delete m_pSteering;
	delete m_pHeadingSmoother;
}

//------------------------------ Update ----------------------------------
//
//  Updates the vehicle's position from a series of steering behaviors
//------------------------------------------------------------------------
double PredetorSpeed;
void Vehicle::Update(double time_elapsed, double Distance, Vehicle* Predetor, double Radian)
{    
	double t=Predetor->Scale().x*2;
	PredetorScale = Predetor->Scale().x;
	PreyScale = this->Scale().x;
	PredetorSpeed = Predetor->Speed();

	Update_Distance=Distance;
	//update the time elapsed
	m_dTimeElapsed = time_elapsed;
	//keep a record of its old position so we can update its cell later
	//in this method
	Vector2D OldPos = Pos();

	Vector2D SteeringForce;
	//calculate the combined force from each steering behavior in the 
	//vehicle's list
	
	
	
	/*
	if(Update_Distance<t){
		if(!this->Steering()->isSeekOn()){
			this->Steering()->EvadeOff();
			this->Steering()->SeekOn();
			p=PointToWorldSpace(Vector2D(cos(-Radian)*500,sin(-Radian)*500),
				this->Heading(),
				this->Side(),
				this->Pos());
			
		}
		World()->SetCrosshair(p);
	}
	*/
	
	
	/*
	if(Vec2DDistance(p, this->Pos()) <15){
			this->Steering()->SeekOn();
			t*=-1;
			p=PointToWorldSpace(Vector2D(cos(t)*50,sin(t)*50),
				this->Heading(),
				this->Side(),
				this->Pos());
			World()->SetCrosshair(p);
	}
	*/

	/*
	if(Update_Distance>t){
		if(this->Steering()->isSeekOn()){
			this->Steering()->SeekOff();
			this->Steering()->EvadeOn(Predetor);
		}
	}
	*/
	
	
	

	//피식자 에너지 소모 계산
	/*
	Vector2D EvadeForce = Steering()->Evade(Predetor); 
	double UsedEnergy = (EvadeForce.Length()+this->Mass()*frictionfactor)*
		(this->m_vVelocity.Length()+(0.5*EvadeForce.Length()*time_elapsed/
		(this->Mass())))*time_elapsed;
	PREY_ENERGY-=UsedEnergy;
	
	if(PREY_ENERGY<0){
		this->SetMaxSpeed(0);
	}
	*/
	



	SteeringForce = m_pSteering->Calculate();
	Vector2D acceleration = (SteeringForce) / m_dMass;
	m_vVelocity += acceleration * time_elapsed;
	//make sure vehicle does not exceed maximum velocity
	m_vVelocity.Truncate(m_dMaxSpeed);
	//update the position
	m_vPos += m_vVelocity * time_elapsed;
	//update the heading if the vehicle has a non zero velocity
	if (m_vVelocity.LengthSq() > 0.00000001)
	{    
		m_vHeading = Vec2DNormalize(m_vVelocity);
		m_vSide = m_vHeading.Perp();
	}
	//EnforceNonPenetrationConstraint(this, World()->Agents());
	//treat the screen as a toroid
	//WrapAround(m_vPos, m_pWorld->cxClient(), m_pWorld->cyClient());
	//update the vehicle's current cell if space partitioning is turned on
	if (Steering()->isSpacePartitioningOn())
	{
		World()->CellSpace()->UpdateEntity(this, OldPos);
	}
	
	if(!this->Steering()->isEvadeOn()){
		double PreyMaxSpeedTime = this->Mass()*this->MaxSpeed()/this->MaxForce();
		double PredetorMaxSpeedTime = Predetor->Mass()*Predetor->MaxSpeed()/Predetor->MaxForce();
		Beta=PredetorMaxSpeedTime - PreyMaxSpeedTime;
		if(Beta<0) Beta=0;
		PreyNeedEnergy = (this->Mass()*0.5)*
			((this->MaxSpeed()*this->MaxSpeed())-(this->Speed()*this->Speed()))+
			(this->Mass()*frictionfactor)* //마찰력
			(this->MaxSpeed()*(Vec2DDistance(this->Pos(),Predetor->Pos())-this->Scale().x-Predetor->Scale().x))/  //시작할때 둘의 거리
			(Predetor->MaxSpeed()- this->MaxSpeed())+
			((this->Mass()*frictionfactor*Beta*this->MaxSpeed()*Predetor->MaxSpeed()*0.5)/
			((Predetor->MaxSpeed()-this->MaxSpeed())));
		this->Energy = PreyNeedEnergy;
		//srand((unsigned)time(NULL));
		//PREY_ENERGY = NeedEnergy * (1+(rand()%100)/100.0);//NeedEnergy;
		
		if(PredetorSafeZone+15+PredetorScale+PreyScale>Vec2DDistance(this->Pos(),Predetor->Pos())){
			this->Steering()->WanderOff();
			this->Steering()->SeekOff();
			this->Steering()->EvadeOn(Predetor);
		}
		else
			this->Steering()->WanderOn();
			
		
	}
	if(this->Steering()->isEvadeOn()){
		Vector2D EvadeForce = this->Steering()->Evade(Predetor); //pursuit return vector2d(arrive)


		double PreyUsedEnergy = (EvadeForce.Length()+this->Mass()*frictionfactor)*
			(this->Speed()+(0.5*EvadeForce.Length()*time_elapsed/
			this->Mass()))*time_elapsed;
		PreyTotalUsedEnergy+=PreyUsedEnergy;//총사용에너지
		this->Energy-=PreyUsedEnergy;
		PREY_ENERGY=this->Energy;
		if(PREY_ENERGY <0){
			this->Steering()->EvadeOff();
			this->SetMaxForce(0.0);
		}else if(PredetorSafeZone+15+Predetor->Speed()*10<(Vec2DDistance(Predetor->Pos(),this->Pos())-Predetor->Scale().x-this->Scale().x)&&
			PredetorSafeZone+WanderRange>(Vec2DDistance(Predetor->Pos(),this->Pos())-Predetor->Scale().x-this->Scale().x)){
			this->Steering()->EvadeOff();
			World()->SetCrosshair(WaterZone);
			this->Steering()->WanderOn();
		}else{
			
		}
		prey_route.push_back(this->Pos());
		
	}
	if(this->Steering()->isSeekOn()){
		prey_route.push_back(this->Pos());
		if(PredetorSafeZone+WanderRange>(Vec2DDistance(Predetor->Pos(),this->Pos())-Predetor->Scale().x-this->Scale().x)){
			this->Steering()->EvadeOff();
			this->Steering()->SeekOff();
			this->Steering()->WanderOn();
		}
	}
	if(this->Steering()->isWanderOn()){
		prey_route.push_back(this->Pos());
		if(PredetorSafeZone+WanderRange-this->Scale().x<(Vec2DDistance(Predetor->Pos(),this->Pos())-Predetor->Scale().x-this->Scale().x)){
			this->Steering()->WanderOff();
			this->Steering()->SeekOn();
		}
		if(PredetorSafeZone+15+PredetorScale+PreyScale>Vec2DDistance(this->Pos(),Predetor->Pos())){
			this->Steering()->WanderOff();
			this->Steering()->SeekOff();
			this->Steering()->EvadeOn(Predetor);
		}
	}
	
	/*
	if(!this->Steering()->isSeekOn() && !Predetor->Steering()->isPursuitOn()){
		this->Steering()->EvadeOff();
		World()->SetCrosshair(WaterZone);
		this->Steering()->SeekOn();
	}
	*/
	
	if (isSmoothingOn())
	{
		m_vSmoothedHeading = m_pHeadingSmoother->Update(Heading());
	}

	
	
} 
bool EnergyControll=true;
void Vehicle::Update(double time_elapsed, std::vector<Vehicle*>& Target)
{    
	
	//update the time elapsed
	m_dTimeElapsed = time_elapsed;
	PredetorPos = this->Pos();
	//keep a record of its old position so we can update its cell later
	//in this method
	Vector2D OldPos = Pos();


	Vector2D SteeringForce;

	//calculate the combined force from each steering behavior in the 
	//vehicle's list
	SteeringForce = m_pSteering->Calculate();

	//Acceleration = Force/Mass
	Vector2D acceleration = SteeringForce / m_dMass;

	//update velocity
	m_vVelocity += acceleration * time_elapsed; 

	//make sure vehicle does not exceed maximum velocity
	m_vVelocity.Truncate(m_dMaxSpeed);

	//update the position
	m_vPos += m_vVelocity * time_elapsed;

	//update the heading if the vehicle has a non zero velocity
	if (m_vVelocity.LengthSq() > 0.00000001)
	{    
		m_vHeading = Vec2DNormalize(m_vVelocity);

		m_vSide = m_vHeading.Perp();
	}

	//EnforceNonPenetrationConstraint(this, World()->Agents());

	//treat the screen as a toroid
	//WrapAround(m_vPos, m_pWorld->cxClient(), m_pWorld->cyClient());

	//update the vehicle's current cell if space partitioning is turned on
	if (Steering()->isSpacePartitioningOn())
	{
		World()->CellSpace()->UpdateEntity(this, OldPos);
	}

	if (isSmoothingOn())
	{
		m_vSmoothedHeading = m_pHeadingSmoother->Update(Heading());
	}
	
	if(!Steering()->isPursuitOn()){
		for(int i=0; i<Target.size()-1; i++){
			double PreyMaxSpeedTime = Target[i]->Mass()*Target[i]->MaxSpeed()/Target[i]->MaxForce();
			double PredetorMaxSpeedTime = this->Mass()*this->MaxSpeed()/this->MaxForce();
			Beta=PredetorMaxSpeedTime - PreyMaxSpeedTime;
			if(Beta<0) Beta=0;

			NeedEnergy = (Mass()*0.5)*
				((this->MaxSpeed()*this->MaxSpeed())-(this->Speed()*this->Speed()))+
				((this->Mass()*frictionfactor)* //마찰력
				(this->MaxSpeed()*(Vec2DDistance(this->Pos(),Target[i]->Pos())-this->Scale().x-Target[i]->Scale().x))/  //시작할때 둘의 거리
				(this->MaxSpeed()-Target[i]->MaxSpeed()))+
				((this->Mass()*frictionfactor*Beta*this->MaxSpeed()*Target[i]->MaxSpeed()*0.5)/
				((this->MaxSpeed()-Target[i]->MaxSpeed())));
			double e=((this->Mass()*frictionfactor*Beta*this->MaxSpeed()*Target[i]->MaxSpeed()*0.5)/
				((this->MaxSpeed()-Target[i]->MaxSpeed())));
			PredetorSafeZone = ((this->MaxSpeed()-Target[i]->MaxSpeed())*
				(this->Energy-(0.5*this->Mass()*
				(this->MaxSpeed()*this->MaxSpeed()-(this->Speed()*this->Speed())))-e))/
				(this->MaxSpeed()*this->Mass()*frictionfactor);

			if(this->Energy>=NeedEnergy){
				//this->Steering()->PursuitOn(Target[i]);
			}
		}
		/*
		if(EnergyControll==false){
			this->Energy-=30;
			if(this->Energy<10000) EnergyControll=true;	
			return;
		}else if(EnergyControll==true){
			this->Energy+=30;
			if(this->Energy>80000) EnergyControll=false;	
			return;
		}
		*/

	}
	if(Steering()->isPursuitOn()){
		for(int i=0; i<Target.size()-1; i++){
			Vector2D PursuitForce = Steering()->Pursuit(Target[i]); //pursuit return vector2d(arrive)
		}

		double UsedEnergy = (PursuitForce.Length()+this->Mass()*frictionfactor)*
			(this->Speed()+(0.5*PursuitForce.Length()*time_elapsed/
			this->Mass()))*time_elapsed;

		TotalUsedEnergy+=UsedEnergy;//총사용에너지
		this->Energy-=UsedEnergy;//남은에너지

		
		if(this->Energy<0){
			this->Steering()->PursuitOff();
			this->SetMaxForce(0.0);
		}else{
			predetor_route.push_back(this->Pos());
		}
		
		
	}
	PREDETOR_ENERGY = this->Energy;
}


//-------------------------------- Render -------------------------------------
//-----------------------------------------------------------------------------
void Vehicle::Render()
{ 
	//a vector to hold the transformed vertices
	
	static std::vector<Vector2D>  m_vecVehicleVBTrans;
	
	//render neighboring vehicles in different colors if requested

	

	if (m_pWorld->RenderNeighbors())
	{
		if (ID() == 0) gdi->RedPen();
		else if(IsTagged()) gdi->GreenPen();
		else gdi->BluePen();
	}

	else
	{
		gdi->BluePen();
	}

	if (Steering()->isInterposeOn())
	{
		gdi->RedPen();
	}

	if (Steering()->isHideOn())
	{
		gdi->GreenPen();
	}

	if (isSmoothingOn())
	{ 
		m_vecVehicleVBTrans = WorldTransform(m_vecVehicleVB,
			Pos(),
			SmoothedHeading(),
			SmoothedHeading().Perp(),
			Scale());
	}

	else
	{
		
		m_vecVehicleVBTrans = WorldTransform(m_vecVehicleVB,
			Pos(),
			Heading(),
			Side(),
			Scale());
			
	}

	
	

	//render any visual aids / and or user options
	if (m_pWorld->ViewKeys())
	{
		Steering()->RenderAids();
	}

	gdi->TextAtPos(5, 20, "추격자필요에너지        : "+ttos(NeedEnergy));
	gdi->TextAtPos(5, 80, "추격자사용에너지  : "+ttos(TotalUsedEnergy));
	gdi->TextAtPos(5, 100,"추격자남은에너지  : "+ttos(PREDETOR_ENERGY));
	gdi->TextAtPos(5, 220,"희생자필요에너지  : "+ttos(PREY_ENERGY));
	gdi->TextAtPos(5, 240,"SafeZone  : "+ttos(PredetorSafeZone));
	gdi->TextAtPos(5, 260,"희생자남은에너지  : "+ttos(PredetorScale));
	
	if(prey_route.size()!=0){
		
		gdi->GreenPen();
		gdi->DrawDot(prey_route[prey_route.size()-1].x,prey_route[prey_route.size()-1].y,0.1);
	}
	if(predetor_route.size()!=0){
		gdi->GreenPen();
		gdi->Circle(predetor_route[predetor_route.size()-1].x,predetor_route[predetor_route.size()-1].y,20.75);
	}
	
	gdi->RedPen();
	gdi->TransBrush();
	for(int i=0; i<predetor_route.size();i++){
		gdi->Circle(predetor_route[i].x,predetor_route[i].y-5,1);
	}
	gdi->BluePen();
	for(int i=0; i<prey_route.size(); i++){
		
		gdi->DrawDot(prey_route[i].x,prey_route[i].y,0.01);
	}
	gdi->RedPen();
	gdi->Circle(PredetorPos,PredetorSafeZone+PredetorScale);
	gdi->GreyPen();
	gdi->Circle(PredetorPos,PredetorSafeZone+WanderRange+PredetorScale+PredetorScale*0.5);
	gdi->BluePen();
	gdi->Circle(PredetorPos,PredetorSafeZone+PredetorScale+15+PredetorSpeed*8);
	gdi->ClosedShape(m_vecVehicleVBTrans);
	
}


//----------------------------- InitializeBuffer -----------------------------
//
//  fills the vehicle's shape buffer with its vertices
//-----------------------------------------------------------------------------
void Vehicle::InitializeBuffer()
{
	const int NumVehicleVerts = 3;

	Vector2D vehicle[NumVehicleVerts] = {Vector2D(-1.0f,0.6f),
		Vector2D(1.0f,0.0f),
		Vector2D(-1.0f,-0.6f)};

	//setup the vertex buffers and calculate the bounding radius
	for (int vtx=0; vtx<NumVehicleVerts; ++vtx)
	{
		m_vecVehicleVB.push_back(vehicle[vtx]);
	}
}
